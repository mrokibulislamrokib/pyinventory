from django.db import models
from category.models import CategoryModel
from subcategory.models import SubcategoryModel
# Create your models here.
class ProductModel(models.Model):
    product_id=models.IntegerField()
    serial_number=models.CharField(max_length=50)
    category= models.ForeignKey(CategoryModel,on_delete=models.PROTECT)
    subcategory= models.ForeignKey(SubcategoryModel,on_delete=models.PROTECT)
    name=models.CharField(max_length=50)
    purchase_price=models.CharField(max_length=50)
    selling_price=models.CharField(max_length=50)
    note=models.TextField()
    stock_qty=models.IntegerField()

    def __str__(self):
        return self.serial_number