# Generated by Django 2.0.1 on 2018-01-13 02:02

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TaxModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tax_id', models.IntegerField()),
                ('tax_code', models.CharField(max_length=50)),
                ('name', models.CharField(max_length=50)),
                ('tax_percentage', models.CharField(max_length=50)),
                ('notes', models.TextField()),
            ],
        ),
    ]
