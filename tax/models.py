from django.db import models

# Create your models here.
class TaxModel(models.Model):
    tax_id=models.IntegerField()
    tax_code=models.CharField(max_length=50)
    name=models.CharField(max_length=50)
    tax_percentage=models.CharField(max_length=50)
    notes=models.TextField()

    def __str__(self):
        return self.name