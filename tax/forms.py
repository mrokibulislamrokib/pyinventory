from  django import forms
from .models import TaxModel
class TaxForm(forms.ModelForm):
    class Meta:
        model=TaxModel
        fields='__all__'