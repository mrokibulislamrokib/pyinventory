from django.db import models
from category.models import CategoryModel
# Create your models here.
class SubcategoryModel(models.Model):
    subcategory_id=models.IntegerField()
    name=models.CharField(max_length=50)
    description=models.TextField()
    category= models.ForeignKey(CategoryModel,on_delete=models.PROTECT)

    def __str__(self):
      return  self.name