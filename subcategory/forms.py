from  django import forms
from .models import SubcategoryModel
class SubCategoryForm(forms.ModelForm):
    class Meta:
        model=SubcategoryModel
        fields='__all__'