# Generated by Django 2.0.1 on 2018-01-13 02:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DamagedProductModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dproduct_id', models.IntegerField()),
                ('quantity', models.IntegerField()),
                ('note', models.TextField()),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='product.ProductModel')),
            ],
        ),
    ]
