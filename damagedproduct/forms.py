from  django import forms
from .models import DamagedProductModel
class DamagedProductForm(forms.ModelForm):
    class Meta:
        model=DamagedProductModel
        fields='__all__'