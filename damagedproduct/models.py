from django.db import models
from product.models import ProductModel
# Create your models here.
class DamagedProductModel(models.Model):
    dproduct_id=models.IntegerField()
    product= models.ForeignKey(ProductModel,on_delete=models.PROTECT)
    quantity=models.IntegerField()
    note=models.TextField()

    def __str__(self):
        return self.quantity