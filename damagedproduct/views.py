from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required
from django.http import  HttpResponse,Http404
from django.views.generic import View
from django.views.generic.base import TemplateView,TemplateResponseMixin,ContextMixin
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView,UpdateView,DeleteView,ModelFormMixin
from django.utils.decorators import method_decorator
from django.shortcuts import render

from .forms import DamagedProductForm
# Create your views here.
def dproudct_list(request):
    return render(request,'Damagedproduct/damagedproduct-list.html')


class DproductCreateView(SuccessMessageMixin,CreateView):
    template_name = ''
    form_class = ''
    success_message = ''
    def form_valid(self, form):
      return  super(DproductCreateView,self).form_valid(self,form)

    def get_success_url(self):
        return ''
    def get_success_message(self, cleaned_data):
        return ''

class DproductDeleteView(DeleteView):
    model = ''
    def get_success_url(self):
        return ''

class DproductUpdateView(UpdateView):
    model = ''
    form_class = ''
    template_name = ''

class DproductListView(ListView):
    model = ''