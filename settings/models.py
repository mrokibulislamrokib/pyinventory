from django.db import models

# Create your models here.
from django.db import models

# Create your models here.
class SettingsModel(models.Model):
    settings_id=models.IntegerField()
    type=models.CharField(max_length=50)
    description=models.TextField()

    def __str__(self):
       return self.settings_id