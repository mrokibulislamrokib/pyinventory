from django.db import models

# Create your models here.
class CustomerModel(models.Model):
    customer_id=models.IntegerField()
    customer_code=models.CharField(max_length=50)
    name=models.CharField(max_length=100)
    email=models.CharField(max_length=100)
    password=models.CharField(max_length=20)
    phone=models.CharField(max_length=50)
    address=models.TextField()
    gender=models.CharField(max_length=50)
    discount_percentage=models.CharField(max_length=50)

    def __str__(self):
        return self.name