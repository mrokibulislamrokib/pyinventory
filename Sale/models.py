from django.db import models
from customer.models import CustomerModel
# Create your models here.
class SaleModel(models.Model):
    invoice_id=models.IntegerField()
    invoice_code=models.CharField(max_length=50)
    invoice_entries=models.TextField()
    discount_percentage=models.IntegerField()
    vat_percentage=models.IntegerField()
    sub_total=models.CharField(max_length=100)
    grand_total=models.CharField(max_length=100)
    due=models.CharField(max_length=50)
    customer= models.ForeignKey(CustomerModel,on_delete=models.PROTECT)

    def __str__(self):
        return self.invoice_code