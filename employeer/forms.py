from  django import forms
from .models import EmployeerModel
class EmployeerForm(forms.ModelForm):
    class Meta:
        model=EmployeerModel
        fields='__all__'