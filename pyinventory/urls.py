"""pyinventory URL Configuration
/* , */
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from supplier.views import supplier_list
from subcategory.views import subcategory_list
from settings.views import settings_list
from tax.views import tax_list
from purchase.views import purchase_list,purchase_create
from product.views import product_list
from payment.views import payment_list,customer_payment_list
from employeer.views import employeer_list
from damagedproduct.views import dproudct_list
from customer.views import  customer_list
from category.views import category_list
from subcategory.views import subcategory_list
from Sale.views import sale_list,sale_create
from dashboard.views import home
from Order.views import order_list,order_create
from User.views import user_login
urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^$',home,name='home'),
    url(r'^supplier/$', supplier_list, name='supplier_list'),
    url(r'^settings/$', settings_list, name='settings_list'),
    url(r'^purchase/$', purchase_list, name='purchase_list'),
    url(r'^purchase/create$', purchase_create, name='purchase_create'),
    url(r'^product/$', product_list, name='product_list'),
    url(r'^payment/$', payment_list, name='payment_list'),
    url(r'^customer_payment/$', customer_payment_list, name='customer_payment_list'),
    url(r'^employeer/$', employeer_list, name='employeer_list'),
    url(r'^damagedproduct/$', dproudct_list, name='dproudct_list'),
    url(r'^customer/$', customer_list, name='customer_list'),
    url(r'^category/$', category_list, name='category_list'),
    url(r'^subcategory/$', subcategory_list, name='subcategory_list'),
    url(r'^sale/$', sale_list, name='sale_list'),
    url(r'^sale/create$', sale_create, name='sale_create'),
    url(r'^tax/$',tax_list, name='tax_list'),
    url(r'^order/$',order_list, name='order_list'),
    url(r'^order/create$',order_create, name='order_create'),
    url(r'^login/$', user_login, name='user_login'),
]
