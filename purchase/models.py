from django.db import models
from supplier.models import SupplierModel
# Create your models here.
class PurchaseModel(models.Model):
    purchase_id=models.IntegerField()
    purchase_code=models.CharField(max_length=50)
    supplier= models.ForeignKey(SupplierModel,on_delete=models.PROTECT)
    purchase_entries=models.TextField()

    def __str__(self):
        return self.purchase_code
