from  django import forms
from .models import PurchaseModel
class PurchaseForm(forms.ModelForm):
    class Meta:
        model=PurchaseModel
        fields='__all__'