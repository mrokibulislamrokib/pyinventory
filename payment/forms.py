from  django import forms
from .models import PaymentModel
class PaymentForm(forms.ModelForm):
    class Meta:
        model=PaymentModel
        field='__all__'