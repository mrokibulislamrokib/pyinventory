from django.db import models
from supplier.models import SupplierModel
from customer.models import CustomerModel
from Sale.models import SaleModel
from purchase.models import PurchaseModel
from Order.models import OrderModel
# Create your models here.
class PaymentModel(models.Model):
    payment_id=models.IntegerField()
    type=models.CharField(max_length=50)
    method=models.CharField(max_length=50)
    amount=models.CharField(max_length=50)
    supplier= models.ForeignKey(SupplierModel,on_delete=models.PROTECT)
    customer= models.ForeignKey(CustomerModel,on_delete=models.PROTECT)
    invoice= models.ForeignKey(SaleModel,on_delete=models.PROTECT)
    purchase= models.ForeignKey(PurchaseModel,on_delete=models.PROTECT)
    order= models.ForeignKey(OrderModel,on_delete=models.PROTECT)

    def __str__(self):
        return self.amount