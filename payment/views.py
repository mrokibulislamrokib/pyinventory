from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required
from django.http import  HttpResponse,Http404
from django.views.generic import View
from django.views.generic.base import TemplateView,TemplateResponseMixin,ContextMixin
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView,UpdateView,DeleteView,ModelFormMixin
from django.utils.decorators import method_decorator
from django.shortcuts import render

# Create your views here.
def payment_list(request):
    return render(request,'Payment/payment-list.html')

def customer_payment_list(request):
    return render(request,'Payment/customer-payment.html')

class PaymentCreateView(SuccessMessageMixin,CreateView):
    template_name = ''
    form_class = ''
    success_message = ''
    def form_valid(self, form):
      return  super(PaymentCreateView,self).form_valid(self,form)

    def get_success_url(self):
        return ''
    def get_success_message(self, cleaned_data):
        return ''

class PaymentDeleteView(DeleteView):
    model = ''
    def get_success_url(self):
        return ''

class PaymentUpdateView(UpdateView):
    model = ''
    form_class = ''
    template_name = ''

class PaymentListView(ListView):
    model = ''