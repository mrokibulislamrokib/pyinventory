from django.shortcuts import render
from django.http import HttpResponseRedirect
# Create your views here.

def user_login(request):
    return render(request, 'user/login.html')

def user_logout(request):

    return HttpResponseRedirect('/')