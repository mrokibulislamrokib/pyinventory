from django.db import models
from customer.models import CustomerModel
# Create your models here.

class OrderModel(models.Model):
    order_id=models.IntegerField()
    order_number=models.CharField(max_length=50)
    customer= models.ForeignKey(CustomerModel,on_delete=models.PROTECT)
    order_entries=models.TextField()
    vat_percentage=models.CharField(max_length=50)
    discount_percentage=models.CharField(max_length=50)
    sub_total=models.CharField(max_length=100)
    grand_total=models.CharField(max_length=100)
    due=models.CharField(max_length=50)
    shipping_address=models.TextField()
    order_status=models.IntegerField()
    payment_status=models.IntegerField()
    note=models.TextField()

    def __str__(self):
        return self.order_number