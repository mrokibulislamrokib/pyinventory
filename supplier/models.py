from django.db import models

# Create your models here.

class SupplierModel(models.Model):
    supplier_id=models.IntegerField()
    company=models.CharField(max_length=50)
    name=models.CharField(max_length=50)
    email=models.CharField(max_length=50)
    phone=models.CharField(max_length=50)

    def __str__(self):
        self.name