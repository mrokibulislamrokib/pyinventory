from  django import forms
from .models import SupplierModel
class SupplierForm(forms.ModelForm):
    class Meta:
        model=SupplierModel,
        fields=['company','name','email','phone']

        def __init__(self, *args, **kwargs):
            super(SupplierForm, self).__init__(*args, **kwargs)
            for visible in self.visible_fields():
                visible.field.widget.attrs['class'] = 'form-control'