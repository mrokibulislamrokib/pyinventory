from  django import forms
from .models import CategoryModel

class CategoryForm(forms.ModelForm):
    class Meta:
        model=CategoryModel
        fields=['name','description']

        def __init__(self, *args, **kwargs):
            super(CategoryForm, self).__init__(*args, **kwargs)
            for visible in self.visible_fields():
                visible.field.widget.attrs['class'] = 'form-control'